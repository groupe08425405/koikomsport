# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 14:41:24 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°1 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page3.py">
    <h1 class="introduction">Première question</h1>
    
    <br>
    
    <fieldset>
        <legend><strong><h3>• Votre enfant court-il vite ? •</h3></legend></strong>
        <div class="res">
            <input type="radio" name="vit" value="tresvite" id="tresvite"><label for="tresvite"> Oui, très vite !!</label>
            <br><br>
            <input type="radio" name="vit" value="assvite" id="assvite"><label for="assvite"> Oui, assez vite !</label>
            <br><br>
            <input type="radio" name="vit" value="asslent" id="asslent"><label for="asslent"> Bof, il est pas très rapide...</label>
            <br><br>
            <input type="radio" name="vit" value="escar" id="escar" checked><label for="escar"> Oula non, c'est un escargot mon gosse !!</label>
            <br><br>
            <input type="radio" name="vit" value="ssjambes" id="ssjambes"><label for="ssjambes"> Arrêtez avec vos questions il ne peut pas courir...Il est unijambiste...</label>
        </div>    
    </fieldset>
    <script>
    let vitesse = document.querySelectorAll('input[name="vit"]')
    let couleur = ""
    for (let i = 0; i < vitesse.length; i++) {
    if (vitesse[i].checked) {
        a = vitesse[i].value
        break
        }
    }
    </script>
    <input class="reponses" type="submit" value="Question suivante">
    </body>
    </html>
    """)

