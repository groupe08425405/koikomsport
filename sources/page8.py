# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 17:34:35 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°7 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> QUE CHOISIR ?</strong><br> ••• </h1>
    
    <form method="post" action="page9.py">
    <h1 class="introduction">Question 7777777 :</h1>
    
    <br>
    
    <fieldset>
    <legend><h3>• Sait-il faire de vélo ? •</h3></legend>
    <div class="res">
        <input type="radio" name="velo" value="weel" id="weel"><label for="weel">Oh ça oui ! Il fait des weelings avec son vélo !! </label><br><br>
        <input type="radio" name="velo" value="tschuss" id="tschuss"><label for="tschuss">Il va tout schuss dans les descentes ! </label><br><br>
        <input type="radio" name="velo" value="chute" id="chute"><label for="chute">Bof, il tombe souvent celui-là !</label><br><br>
        <input type="radio" name="velo" value="roulette" id="roulette" checked><label for="roulette">Il a encore besoin de roulettes pour avancer et tenir en équilibre.</label><br><br>
        <input type="radio" name="velo" value="nop" id="nop"><label for="nop">Un vélo ? Lui ? Il sait pas ce que c'est ! </label><br><br>
    </div>
    </fieldset>
    <input class="reponses" type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
