# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:56:51 2024

@author: lmarouby1
"""
import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

print("""<head>
	<meta charset="UTF_8">
	<title> Pré-questionnaire </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
        <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
        <h1 class="banner"> ••• <br><strong> QUE CHOISIR ?</strong><br> ••• </h1>
    <form method="post" action="page2.py">
    """)

# On ne fait pas confiance à l'utilisateur
try :  # on essaie : si ça marche, RAS
    if form.getvalue("name"): #si l'utilisateur a bien rempli le champ username
        name = form.getvalue("name") # on enregistre la valeur transmise
        print(f"""<h1 class="introduction"><strong> {name} est un si joli prénom !</strong></h1>""") # on affiche son nom de façon conviviale
    else :
        raise Exception #sinon, on lève une exception
except:  # Si une exception a été levée
    print("<p>Pseudo non transmis</p>") # On informe l'utilisateur

html="""

    <p class="introduction"> Avant de commencer, nous vous demandons d'agréer à ces quelques conditions afin d'accéder au questionnaire :<br><br>
    <br>
    <fieldset>
            <legend><strong>• CONDITIONS •</strong></legend>    
                <input type="checkbox" name="cond" id="cond1" required><label for="cond1"> Vous acceptez que vos informations soient réutilisées dans le seul but de vous apporter un résultat personnalisé en fonction de vos réponses.</label>
    <br><br>
                <input type="checkbox" name="cond" id="cond2" required><label for="cond2"> Vous acceptez que vos informations soient récupérées puis stockées dans les archives de notre serveur. </label>
    <br><br>
                <input type="checkbox" name="cond" id="cond3"><label for="cond3"> Vous apportez votre soutien dans une collecte de fonds pour la protection des bébés pingouins en Antarctique. </label> <!--Pour rire pas obligatoire--> 
    <br><br>
                <input class="reponses" type="submit" value="Passer au questionnaire !">
    </fieldset>
    </body>
</html>"""

print(html)

echec = 0
ventre = 0
hockey = 0
caisse = 0
a = form.getvalue("handi") # on enregistre la valeur transmise
if a == "O" :
    echec += 100
    
b = form.getvalue("all")
if b == "chlore" :
    hockey = -100
    
c = form.getvalue("main")
if c == "sans" :
    ventre = 50
    caisse = 50
    echec -= 70

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()

fichier = open("resultatsechec.csv", mode="w")
fichier.write (str(echec))
fichier.close()

fichier = open("nom.csv", mode="w")
fichier.write(name)
fichier.close()

