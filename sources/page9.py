# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 17:52:38 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°8 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body class="background666">
    
    <h1 class="grandtitre666"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner666"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page10.py">
    <h1 class="introduction">888 c'est la question n°8</h1>
    <br>
    
    <fieldset>
    <legend><strong><h3 class="pol1">Sait-il faire de vélo ?</h3></strong></legend><br>
    <h3> Ah non, c'est pas ça !</h3><br>
    <h3 class="pol3"> Je sais plus ...</h3><br>
    <h2 class="introduction"> Ah si !!</h2><br>
    <h3 class="pol3"> Bah non ...</h3><br>
    <h3 class="pol4"> Bon tant pis ...</h3><br>
    <h3> Question suivante ?? C'est en bas</h3> <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <h4 class="pol3"> Plus bas encore </h3> <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <h5 class="pol3"> Encore ! Encore ! </h5><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <input class="reponses666" type="submit" value="Question suivante">
    
    </body>
    </html>
    """)
    
a = form.getvalue("velo") # on enregistre la valeur transmise
if a == "weel" :
   quid += 0
   horse += 0
   cycle += 6
   caisse += 0
   ventre += 0
   hockey += 0
elif a == "tschuss" :
   quid += 0
   horse += 0
   cycle += 5
   caisse += 5
   ventre += 0
   hockey += 0
elif a == "chute" :
   quid += 0
   horse += 0
   cycle += 1
   caisse += 3
   ventre += 0
   hockey += 0
elif a == "roulette" :
   quid += 0
   horse += 0
   cycle -= 1
   caisse += 1
   ventre += 0
   hockey += 0
else : 
   quid += 0
   horse += 0
   cycle -= 10
   caisse += 0
   ventre += 1
   hockey += 0
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()