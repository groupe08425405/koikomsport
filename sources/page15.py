# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:09:53 2024

@author: emorisot
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°14 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    
    <body class="background13">
    
    <h1 class="grandtitre13"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner13"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page16.py">
    <h1 class="introduction13">Question n°14 : Question n°14 : Question n°14 : Question n°14 : Question n°14 :</h1>
    <br>
    <fieldset>
    <legend><strong><h3>Est-il un sorcier ?</h3></strong></legend>
        <input type="radio" name="sorc" value="avada" id="avada"> <label for="avada">HA hA ! Je l'attendais cette question ! Mais bièn ssùr il lance soùvênt des AVADA KEDAVRA</label> <br>
        <input type="radio" name="sorc" value="voler" id="voler"> <label for="voler">Il sait seûlment voler avec un balaî et faire voler dès objets. WINGARDIUM LEVIOSA</label> <br>
        
        <br><br><br><h6 class="pol3">...</h6><br><br><br><h6 class="pol3">c'est déjà la fin?</h6><br><br><br><br><h6 class="pol3">ma fin?</h6><br><br>
        
        <input type="radio" name="sorc" value="fourch" id="fourch"> <label for="fourch">Non il sssaît ssseulement parler le fourchelañgue. SSSIA HASS SSETH</label> <br>
        <input type="radio" name="sorc" value="mold" id="mold" checked > <label for="mold">Non c'èst un sîmple moldu comme vous êt moi</label>
        
        <br><br>
    <fieldset>
    <input class="reponses666"type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
a = form.getvalue("heros")
if a == "squi" :
    horse += 3
    cycle += 3
elif a == "cypher" :
    caisse += 3
elif a == "razor" :
    hockey += 3
    quid += 3
    caisse += 2
    ventre += 4
else : 
    hockey += 2
    horse += 2
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()