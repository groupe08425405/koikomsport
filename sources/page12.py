# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 15:03:23 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF-8">
	<title> Question n°11 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body class="background666">
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner666"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page13.py">
    <h1>C'est la question n°11</h1>
    <br>
    <strong>
    <h3>Possédez-vous une autruche de compagnie dans votre foyer ?</h3>
        <input type="radio" name="autruche" value="adore" id="adore"> <label for="adore"> Oui, et on l'adore. </label>
        <br>
        <input type="radio" name="autruche" value="deux" id="deux"> <label for="deux"> Oui, on en a même deux ! Elles s'ennuient quand elles sont seules les autruches</label>
        <br>
        <input type="radio" name="autruche" value="oeuf" id="oeuf" checked > <label for="oeuf"> L'oeuf n'a pas encore éclos mais prochainement. </label>
        <br>
        <input type="radio" name="autruche" value="non" id="non"> <label for="non"> Non, un enfant ça suffit amplement !</label>
        <br><br>
    </strong>
    <input class="reponses666" type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
a = form.getvalue("playmo")
if a == "lego" :
    caisse += 3
    
fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()