# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 15:16:03 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF-8">
	<title> Question n°12 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body class="background666">
    
    <h1 class="grandtitre666"> <strong> P0Lee SP0RTIF REGI0NAL </strong>| @ssociation des pArents d'élèv3s | ateliers olympiques </h1> 
    <h1 class="banner666"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page14.py">
    <h1>Question décembre (Vous savez c'est le 12ème mois de l'année)</h1>
    <br>
    <h3>Quels sont ses goûts musicaux ?</h3>
        <input type="checkbox" name="music" value="patpat" id="patpat"><label for="patpat"> Patrick Sebastien</label>
        <input type="checkbox" name="music" value="keen" id="keen"><label for="keen"> Keen'V</label>
        <input type="checkbox" name="music" value="pok" id="pok"><label for="pok"> Matt Pokora </label>
        <input type="checkbox" name="music" value="wesh" id="wesh"><label for="wesh"> Wejdene </label>
        <input type="checkbox" name="music" value="stein" id="stein" checked><label for="stein"> Rammstein </label>
        <input type="checkbox" name="music" value="autres" id="autres"><label for="autres"> Autres</label>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><h6 class="pol2">EH ! vous tenez le coup???</h6><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><h6 class="pol2">...c'est ce que vous cherchiez?</h6><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><h6 class="pol2">j'ai aimé vos réponses</h6><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br><h6 class="pol2">Je l3s aime aUtant que V0us..§%</h6><br><br><br><br><br><br><br>
    <input class="reponses" type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
a = form.getvalue("autruche")
if a == "adore" :
    horse += 3
elif a == "deux" :
    horse += 5
elif a == "non" :
    hockey += 3
    cycle += 5
    ventre += 5
    

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()