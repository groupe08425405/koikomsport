# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 18:09:23 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°9 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre">Question 666?</h1>
    <h1 class="banner666">Vous acc3ptez que vos inform@tions s0ient réutiliséEs dans le s3ul bUT de vous @pporter un résultat** persoNnalisé en fONction de vo§ rép0nses??</h1>
    
    <form method="post" action="page11.py">
    <h1 class="introduction">J'appelle la question.........................................................................................................................................9 :</h1>
    
    <br>
    
    <div class="res666">
    <h3>Son independance sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="0" name="inde">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Sa générosité sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="1">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son humour sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="2">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Sa combativité sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="3" name="comb">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son côté individualiste sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="4" name="indiv">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son emphatie sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="5">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son caractère grognon sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="6">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son côté pleurnichard sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="7" name="pleur">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h6 class="pol2">vous êtes toujours là?</h6>
    
    <h3>  sur 7 ? Euh 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="8">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>
    
    <h3>Son adorable petit nez sur 10 :</h3>
    0 <input type="range" list="tickmarks" min="0" max="10" value="9">
    <datalist id="tickmarks">
    <option value="0"></option>
    <option value="1"></option>
    <option value="2"></option>
    <option value="3"></option>
    <option value="4"></option>
    <option value="5"></option>
    <option value="6"></option>
    <option value="7"></option>
    <option value="8"></option>
    <option value="9"></option>
    <option value="10"></option>
    </datalist> 10 <br><br>

    
    </div><br><br>
        <input class="reponses666" type="submit" value="Question suivante">
        
        </body>
    </html>
    """)