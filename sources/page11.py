# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 18:32:26 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°10 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page12.py">
    <h1 class="introduction">Enfin leS questi0ns à d3Ux chiffres ! Question dix :</h1>
    
    <br>
   
    <div class="res666">
    <fieldset>
    <legend><strong><h3>•Aime-t-il les Playmobils ?•</h3></strong></legend>
        <label  for="playmo"></label>
        <select class="reponses" name="playmo" id="playmo">
        <option value="o">Oui, il faiT tout un monde avec ces machins.</option>
        <option value="lego">Non il préfère faire des LEGOs.</option>
        <option value="maths">Non, qu'il reste faire ses maths.</option>
        <option value="n">Il aime pas jouer avec des bonhommes, il préfère sa Switch</option>
        <option value="sb"selected >Vous avez donc oublié qu'il n'a pas de bras mon enfant ?!</option>
    </select>
    </div>
    </fieldset>
    <br><br>
    <input class="reponses666" type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
a = form.getvalue("inde")
if int(a) >= 9 :
    quid += 4
    caisse += 5
elif int(a) <= 8 and int(a) >= 5 :
    quid += 3
    caisse += 5
elif int(a) <= 4 and int(a) >= 1 :
    quid += 1
    caisse -= 1
else :
    caisse -= 5
    
b = form.getvalue("comb")
if int(b) >= 9 :
    quid += 5
    hockey += 5
    cycle += 5
elif int(b) <= 8 and int(b) >= 5 :
    quid += 4
    hockey += 4
    cycle += 2
elif int(b) <= 4 and int(b) >= 1 :
    quid += 1
    hockey += 1
else :
    quid -= 3
    hockey -= 3
    
c = form.getvalue("indiv")
if int(c) >= 9 :
    ventre += 5
    horse += 5
elif int(c) <= 8 and int(c) >= 5 :
    horse += 3
    ventre += 3
elif int(c) <= 4 and int(c) >= 1 :
    caisse += 2
    hockey += 1
    cycle += 1
    quid += 1
    ventre += 1
    horse -= 1
else :
    caisse += 3
    hockey += 5
    cycle += 5
    quid += 5
    ventre -= 2
    horse -= 2
    
d = form.getvalue("pleur")
if int(d) >= 9 :
    caisse -= 5
    quid -= 2
    horse -= 2
elif int(d) <= 8 and int(d) >= 5 :
   caisse -= 2
elif int(d) <= 4 and int(d) >= 1 :
    caisse += 1
    quid += 1
    horse += 1
else :
    caisse += 5
    quid += 3
    horse += 3

fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()
