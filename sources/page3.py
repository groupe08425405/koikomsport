# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 15:11:52 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)


fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()


print("""<head>
	<meta charset="UTF_8">
	<title> Question n°2 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page4.py">
    <h1 class="introduction">Deuxième question</h1>
    
    <br>
    
    <fieldset>
    <legend><h3>• A-t-il de bons réflexes ? •</h3></legend>
    <div class="res">
        <input type="radio" name="reflexe" value="wouaw" id="wouaw"><label for="wouaw"> Il attrape la balle avant qu'on l'ait lancé, c'est Lucky Luke !!</label>
        <br><br>
        <input type="radio" name="reflexe" value="ptm" id="ptm"><label for="ptm"> Oui, ses réflexes sont bons !</label>
        <br><br>
        <input type="radio" name="reflexe" value="bof" id="bof"><label for="bof"> Bof...</label>
        <br><br>
        <input type="radio" name="reflexe" value="escar" id="escar" checked><label for="escar"> Je vous l'ai deja dit c'est un escargot !</label>
        <br><br>
        <input type="radio" name="reflexe" value="aprlanc" id="aprlanc"><label for="aprlanc"> C'est le pire de tous, il a du ping d'au moins 5 secondes...</label>
    </div>
    </fieldset>
    
    <input class="reponses"type="submit" value="Question suivante">
    
    </body>
    </html>
    """)
    

a = form.getvalue("vit") # on enregistre la valeur transmise
if a == "tresvite" :
    quid = 5
    ventre += 3
    caisse += 0
elif a == "assvite" :
    quid = 4
    ventre += 2
    caisse += 0
elif a == "asslent" :
    quid = 2
    ventre += 1
    caisse += 0
elif a == "escar" :
    quid = -2
    ventre -= 1
    caisse += 0
else : 
    caisse += 7
    quid = 0
    ventre += 0
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()




