# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 11:31:48 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°5 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page7.py">
    <h1 class="introduction">Cinquième question</h1>
    
    <br>
    
    <fieldset>
    <legend><h3>• Est-il créatif ? •</h3></legend>
    <div class="res">
        <input type="checkbox" name="crea" value="dessin" id="dessin"><label for="dessin"> Il fait de très beaux dessins et de très beaux designs</label><br><br>
        <input type="checkbox" name="crea" value="histoire" id="histoire"><label for="histoire"> Il adore inventer des histoires et des blagues</label><br><br>
        <input type="checkbox" name="crea" value="deguiser" id="deguiser"><label for="deguiser"> Il se déguise à sa façon et déguise nos animaux aussi</label><br><br>
        <input type="checkbox" name="crea" value="lego" id="lego"><label for="lego"> Il fabrique de magnifiques maisons avec ses LEGOs</label><br><br>
        <input type="checkbox" name="crea" value="rien" id="rien" checked><label for="rien"> Il ne fait strictement rien de ses mains et ne sait rien faire</label> 
    <br><br>
    </div>
    </fieldset>
    <input class="reponses" type="submit" value="Question suivante">
    
    </body>
    </html>
    """)
    
a = form.getvalue("saut") # on enregistre la valeur transmise
if int(a) == 200 :
   quid += 3
   horse += 10
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 0
elif int(a) <= 190 and int(a) >= 100 :
   quid += 2
   horse += 8
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 0
elif int(a) <= 90 and int(a) >= 50 :
   quid += 1
   horse += 5
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 0
elif int(a) <= 40 and int(a) != 0 :
   quid += 0
   horse -= 2
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 0
else : 
   quid += 0
   horse -= 5
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 0
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()