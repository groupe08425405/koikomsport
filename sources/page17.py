# -*- coding: utf-8 -*-
"""
Created on Mon Mar 18 18:37:04 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()


fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

fichier = open("resultatsechec.csv", mode="r")
echec = (int(fichier.read()))
fichier.close()

fichier = open("nom.csv", mode="r")
name = (fichier.read())
fichier.close()

a = form.getvalue("delinq")
if a == "prison" :
    caisse += 5
    hockey += 5
elif a == "casier" :
    horse += 3
    cycle += 5
elif a =="vol" :
    quid += 3
    hockey +=3
else :
    cycle += 2
    hockey += 2
    caisse += 1

resultats = [quid, ventre, caisse, horse, cycle, hockey, echec]

resultats_max = max(resultats)

resultats_index = resultats.index(resultats_max)
print("""<!DOCTYPE html>
<html lang="fr">""")
if resultats_index == 0: # quiddich
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • QUIDDITCH </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    <form method="post" action="index.py">
        
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> QUIDDITCH MOLDU </strong><br> ••• </h1>
    
    <a href = "https://fr.wikipedia.org/wiki/Quidditch_moldu" title="QUADBALL" target=blank> <img class="images" src="../images/quidditch.jpg"> </a>
    <fieldset>
    <legend> <h1> • C'EST QUOI ? • </h1> </legend>
    <div class="def" > {name} court vite et est habile alors le Quidditch moldu est fait pour lui(elle) !
    Le Quidditch moldu ou, pour les impies qui n’aiment pas Harry Potter, quadball, a été créé en 2005.<br>
    La partie oppose 2 équipes de 7 joueurs.Trois anneaux disposés à des hauteurs différentes, faisant office de buts, sont disposés de part et d'autre du terrain ovale.<br>
    Le but d'un match est d'obtenir plus de points que l'autre équipe en marquant des buts et en attrapant le « vif d'or » (une balle de tennis transportée par un joueur sans balai) pour mettre fin au match. 
    <br><br>
    </div>
    </fieldset>
    <br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici">
    </body>
    </html>
    """

elif resultats_index == 1: # ventriglisse
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • VENTRIGLISSE </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
        <form method="post" action="index.py">
    </head>
        
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> VENTRIGLISSE </strong><br> ••• </h1>
    
    <a href = "https://fr.wikipedia.org/wiki/Ventriglisse" title="VENTRIGLISSE" target=blank> <img class="images" src="../images/ventriglisse.jpg"> </a>
    
    <fieldset>
    <legend><h1>• C'EST QUOI ? •</h1></legend>
    <div class="def"> {name} est casse-cou et adepte de sensations fortes, le ventriglisse est fait pour votre diablotin(e) !
    Le ventriglisse est d’abord un jouet américain introduit au commerce en 1961.
    Des compétitions ont vu le jour où le but est de s’élancer et se jeter à plat ventre sur une toile de plastique savonneuse posée au sol, dans une pente, tout en se laissant glisser. {name} réussira-t-il(elle) à glisser le plus loin possible ?
    </div>
    <br><br>
    </fieldset>
    <br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici"> 
    """

elif resultats_index == 2 : # caisse à savon
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • CAISSE A SAVON </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
        <form method="post" action="index.py">
    </head>
        
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> CAISSE A SAVON </strong><br> ••• </h1>
    
    <a href = "https://fr.wikipedia.org/wiki/Caisse_%C3%A0_savon" title="CAISSE A SAVON" target=blank> <img class="images" src="../images/caisse.jpg"> </a>
    
    <fieldset>
    <legend><h1> • C'EST QUOI ? • </h1></legend>
    <div class="def" >{name} adore monopoliser la télévision familiale pour regarder les courses de Formule 1 alors la course de caisse à savon est faite pour votre mini Lewis Hamilton !
    La caisse à savon est néé aux Etats-Unis en 1933.
    Votre enfant devra créé lui(elle)-même son bolide. Le but est d’arriver à la ligne d’arrivée en premier sans casse ! Saura-t-il(elle) créer un véhicule beau et solide à la fois ? Parce que oui ! Il ne suffit pas d’arriver le premier il faut également la caisse à savon la plus séduisante.
    <br><br>
    </fieldset>
    <br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici"> 
    """

elif resultats_index == 3 : # hobby horsing
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • HOBBY HORSING </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
        <form method="post" action="index.py">
    </head>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> HOBBY HORSING </strong><br> ••• </h1>
    
    <a href = "https://en.wikipedia.org/wiki/Hobby_horsing" title="HOBBY HORSING" target=blank> <img class="images" src="../images/hobby.jpg"> </a>
    
    <fieldset>
    <legend><h1> • C'EST QUOI ? • </h1></legend>
    <div class="def">{name} court vite, saute très haut et est très souvent dans sa bulle, le hobby horsing est fait pour lui(elle) !
    Le hobby horsing est né en Finlande en 2002 et est reconnu comme sport depuis 2017.
    Ce sport recopie l’équitation… mais sans cheval ! Eh oui ! {name} devra effectuer des séquences de mouvements similaires à celles du saut d’obstacles ou du dressage mais avec un « cheval de bâton » à la place du cheval.
    <br><br>
    </div>
    </fieldset>
    <br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici">
    """

elif resultats_index == 4 : # cycle-ball
     html = f"""
     <head>
     	<meta charset="UTF_8">
     	<title> KOIKOMSPORT • CYCLE-BALL </title>
         <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
         <form method="post" action="index.py">
         </head>
         
     <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
     <h1 class="banner"> ••• <br><strong> CYCLE-BALL </strong><br> ••• </h1>
     
     <a href = "https://fr.wikipedia.org/wiki/Cycle-ball" title="CYCLE-BALL" target=blank> <img class="images" src="../images/cycle.jpg"> </a>
     
     <fieldset>
     <legend><h1> • C'EST QUOI ? • </h1></legend>
     <dic class="def">{name} est fan de bécanes non polluantes et de football ? Inscrivez le(la) au cycle ball !!!
     Le cycle ball a été créé par un américain en 1893.
     Les matchs opposent 2 équipes de 2 cyclistes. Les règles sont les mêmes qu’au foot seulement on y joue sur ... un vélo ! Le joueur contrôle le ballon seulement à l’aide de son vélo ou de sa tête, seul le gardien a le droit de toucher le ballon avec ses mains pour l’arrêter.
     <br><br>
     </fieldset>
     </div>
     <br><br>
     Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici"> 
     """

elif resultats_index == 5 : # hockey subaquatique
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • HOCKEY SUBAQUATIQUE </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <form method="post" action="index.py">
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> QUE CHOISIR ?</strong><br> ••• </h1>
    
    <a href = "https://fr.wikipedia.org/wiki/Hockey_subaquatique" title="HOCKEY" target=blank> <img class="images" src="../images/hockey.jpg"> </a>
    
    <fieldset>
    <legend><h1>• C'EST QUOI ? •</h1></legend>
    <div class="def">{name} sait nager comme un poisson et sait retenir sa respiration tel une sirène ? Le hockey sous-marin est fait pour votre petit piranha !
    Le hockey sous-marin ou, pour les plus savants, le hockey subaquatique oppose 2 équipes de 6 joueurs. Le but est, en apnée, de pousser un palet à l’aide d’une crosse spécifique pour le faire rentrer dans le but adverse, les buts étant disposés d’un bout à l’autre du fond de la piscine.
    </div>
    <br><br>
    </fieldset>
    <br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class="reponses" value="Cliquez-ici">
    </body>
    </html>
    """

else : # echec
    html = f"""
    <head>
    	<meta charset="UTF_8">
    	<title> KOIKOMSPORT • ECHEC </title>
        <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
        <form method="post" action="index.py">
    </head>
    <body>    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> ECHEC </strong><br> ••• </h1>
    
    <a href = "https://fr.wikipedia.org/wiki/%C3%89checs" title="ECHEC" target=blank> <img class="images" src="../images/echec.jpg"> </a>
    
    <fieldset>
    <legend><h1>• C'EST QUOI ? •</h1></legend>
    <div class="def"> {name} est atteint d'un handicap trop important pour participer à un sport que nous proposons. Mais étant dans la sympathie avec vous et nous souhaitons 
    quand même que {name} participe à une activité, un sport intellectuel : les échecs !
    Ce jeu très connu met en place des pions, des tours, des cavaliers, des fous, des reines et des rois; noirs ou blancs suivant le choix préalable.
    {name} sera-t-il le futur prodige de l'échiquier, le nouveau <a href = "https://fr.wikipedia.org/wiki/Magnus_Carlsen" target="_blank" > Magnus Carlsen </a> ?
    </div>
    </fieldset>
    </body>
    </html>
    <br><br><br><br>
    Vous voulez refaire ce test pour votre second enfant : <input type="submit" class ="reponses" value="Cliquez-ici"> 
    """
    
print(html)