# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:55:43 2024

@author: lmarouby1
"""


import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF_8">
	<title> KoiKomSport </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
</head>

<body>
	<h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> QUE CHOISIR ?</strong><br> ••• </h1>
	
    <p class="introduction"> <strong>Bienvenue à vous très chers parents !</strong> <br>
    Ce site internet intéractif est mis à votre disposition afin de choisir l'activité sportive idéale pour votre enfant (jusqu'à ses 18ans) près de chez vous. Ci-dessous se trouve un formulaire où vous pourrez rentrer les caractéristiques particulières de votre enfant avant de passer au questionnaire : </p>
 
    <form method="post" action="page1.py">
    
    <fieldset>
        <legend><strong>•FICHE D'IDENTITE•</strong></legend>
            <label for="name" class="questions" >Le prénom de votre enfant :</label><input class="reponses" type="text" name="name" id="name" placeholder="Ex: Zurvan" required />
    
            <label "for="age" class="questions" >L'âge de votre enfant :</label><input class="reponses" type="number" name="age" id="age" min="0" max="18"required/>
    <br>
            <label "for="sexe" class="questions"> Son sexe :</label> 
                    <select  name="sexe" id="sexe">
                        <option value="h" selected>Homme</option>
                        <option value="f">Femme</option>
                        <option value="nb">Non binaire</option>
                        <option value="q">Queer</option>
                        <option value="d">Dragon</option>
                    </select>
    <br>
            <label for="taille" class="questions" >Sa taille (en cm) :</label><input class="reponses" type="number" name="taille" id="taille" min="50" max="210"required/>
    </fieldset>
    
    <fieldset>
        <legend><strong>•SANTE•</strong></legend>
            <p class="questions">A-t-il des allergies ?</p>
                <input type="checkbox" name="po" id="po"/><label for="po">Pollen</label>
                <input type="checkbox" name="chlore" id="chlore"/><label for="chlore">Chlore</label>
                <input type="checkbox" name="foin" id="foin"/><label for="foin">Foin</label>
                <input type="checkbox" name="autre" id="autre" /><label for="autre">Autres</label>
                <input type="checkbox" name="non" id="non" checked/><label for="non">Non</label>
    
            <p class="questions">Souffre-t-il d'un handicap ?</p>
                <input type="radio" name="handi" id ="O" checked/><label for="O">Oui</label>
                <input type="radio" name="handi" id ="N"/><label for="N">Non</label>
                <br>
    </fieldset>

    <fieldset>
        <legend><strong>•AUTRES•</strong></legend>
            <p class="questions"> Est-il :</p>  
                <input type="radio" name="main" value="droite" id="droite"><label for="droite">Droitier</label>
                <input type="radio" name="main" value="gauche" id="gauche"><label for="gauche">Gaucher</label>
                <input type="radio" name="main" value="ambi" id="ambi"><label for="ambi">Ambidextre</label>
                <input type="radio" name="main" value="sans" id="sans" checked><label for="sans">Sans bras</label>
    </fieldset>
    <br>
    <input class="reponses" type="submit" value="Suivant !"></legend></p>
    </form>
</body>
</html>"""

print(html)