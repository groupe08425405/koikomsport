# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 11:20:18 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey=int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°4 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page6.py">
    <h1 class="introduction">Quatrième question</h1>
    
    <br>
    
    <fieldset>
    <legend><h3>• A quelle hauteur saute-t-il ? (en centimètres) •</h3></legend>
        <p class="jaugeV">200
        <br>
        <input class="jaugeV" name="saut" type="range" list="tickmarks" orient="vertical" min="10" max="200" value="30" step="10">
        <datalist id="tickmarks">
        <option value="0"></option>
        <option value="10"></option>
        <option value="20"></option>
        <option value="30"></option>
        <option value="40"></option>
        <option value="50"></option>
        <option value="60"></option>
        <option value="70"></option>
        <option value="80"></option>
        <option value="90"></option>
        <option value="100"></option>
        <option value="110"></option>
        <option value="120"></option>
        <option value="130"></option>
        <option value="140"></option>
        <option value="150"></option>
        <option value="160"></option>
        <option value="170"></option>
        <option value="180"></option>
        <option value="190"></option>
        <option value="200"></option>
        </datalist>
        10</p>
    </fieldset>
    
    <input class="reponses" type="submit" value="Question suivante">
    
    </body>
    </html>
    """)
    
a = form.getvalue("respi") # on enregistre la valeur transmise
if int(a) == 180 :
   quid += 0
   horse += 0
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 8
elif int(a) <= 170 and int(a) >= 100 :
   quid += 0
   horse += 0
   cycle += 0
   caisse += 0
   ventre += 0
   hockey += 5
elif int(a) <= 90 and int(a) >= 40 :
   quid += 0
   cycle += 0
   caisse += 0
   ventre += 0
   horse += 0
   hockey += 3
elif int(a) <= 30 and int(a) != 0 :
   quid -= 0
   horse += 0
   cycle += 0
   caisse -= 0
   ventre += 0
   hockey -=1
else : 
   quid -= 0
   horse += 0
   cycle += 0
   caisse -= 0
   ventre += 0
   hockey -= 3
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()