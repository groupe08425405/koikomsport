# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 11:43:37 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()

fichier = open("resultatshorse.csv", mode="r")
horse=int(fichier.read())
fichier.close()

fichier = open("resultatscycle.csv", mode="r")
cycle=int(fichier.read())
fichier.close()

fichier = open("resultatshockey.csv", mode="r")
hockey = int(fichier.read())
fichier.close()

print("""<head>
	<meta charset="UTF_8">
	<title> Question n°6 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page8.py">
    <h1 class="introduction">Question sii!!iix</h1>
    
    <br>
    <fieldset>
    <legend><h3>• C'est quoi sa couleur préférée ? •</h3></legend>
        <input class="res" type="color" name="color" value="#4A412A"/><br><br>
    </fieldset>
    <input class="reponses" type="submit" value="Question suivante">
    </body>
    </html>
    """)
    
a = form.getvalue("crea") # on enregistre la valeur transmise
if a == "dessin" :
   quid += 0
   horse += 5
   cycle += 0
   caisse += 4
   ventre += 0
   hockey += 0
elif a == "histoire" :
   quid += 0
   horse += 3
   cycle += 0
   caisse += 1
   ventre += 0
   hockey += 0
elif a == "deguiser" :
   quid += 1
   horse += 5
   cycle += 0
   caisse += 3
   ventre += 0
   hockey += 0
elif a == "lego" :
   quid += 0
   horse += 1
   cycle += 0
   caisse += 6
   ventre += 0
   hockey += 0
else : 
   quid -= 2
   horse -= 3
   cycle += 0
   caisse -= 5
   ventre += 3
   hockey += 0
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatcycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()

fichier = open("resultatshockey.csv", mode="w")
fichier.write (str(hockey))
fichier.close()