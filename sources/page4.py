# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 15:38:49 2024

@author: MORISOT Emilio
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

fichier = open("resultatsquid.csv", mode="r")
quid=int(fichier.read())
fichier.close()

fichier = open("resultatsventre.csv", mode="r")
ventre=int(fichier.read())
fichier.close()

fichier = open("resultatscaisse.csv", mode="r")
caisse=int(fichier.read())
fichier.close()


print("""<head>
	<meta charset="UTF_8">
	<title> Question n°3 </title>
    <link id="css" rel="stylesheet" type="text/css" href="Styles1/style.css"/>
    </head>
    <body>
    
    <h1 class="grandtitre"> <strong> POLE SPORTIF REGIONAL </strong>| association des parents d'élèves | ateliers olympiques </h1> 
    <h1 class="banner"> ••• <br><strong> KOI KOM SPORT ?</strong><br> ••• </h1>
    
    <form method="post" action="page5.py">
    <h1 class=introduction>Troisième question</h1>
    
    <br>
    
    <fieldset>
    <legend><h3>• Combien de temps peut-il retenir sa respirtion ? (en secondes) •</h3></legend>
        <p class="jauge">0
        <input class="jauge" type="range" name="respi" list="tickmarks" min="0" max="180" value="30" step="10">
        <datalist id="tickmarks">
        <option value="0"></option>
        <option value="10"></option>
        <option value="20"></option>
        <option value="30"></option>
        <option value="40"></option>
        <option value="50"></option>
        <option value="60"></option>
        <option value="70"></option>
        <option value="80"></option>
        <option value="90"></option>
        <option value="100"></option>
        <option value="110"></option>
        <option value="120"></option>
        <option value="130"></option>
        <option value="140"></option>
        <option value="150"></option>
        <option value="160"></option>
        <option value="170"></option>
        <option value="180"></option>
        </datalist> 180</p>
        <br>
    </fieldset>
    
    <input class="reponses" type="submit" value="Question suivante">
    </body>
    </html>
    """)

a = form.getvalue("reflexe") # on enregistre la valeur transmise
if a == "wouaw" :
   quid += 3
   horse = 2
   cycle = 5
   caisse += 3
   ventre += 0
elif a == "ptm" :
   quid += 2
   horse = 1
   cycle = 4
   caisse += 2
   ventre += 0
elif a == "bof" :
   quid += 1
   cycle = 2
   caisse += 1
   ventre += 0
   horse = 0
elif a == "escar" :
   quid -= 2
   horse = -1
   cycle = -2
   caisse -= 4
   ventre += 0
else : 
   ventre += 3
   caisse += 0
   quid += 0
   cycle += 0
   horse += 0
    
fichier = open("resultatsquid.csv", mode="w")
fichier.write (str(quid))
fichier.close()

fichier = open("resultatsventre.csv", mode="w")
fichier.write (str(ventre))
fichier.close()

fichier = open("resultatscaisse.csv", mode="w")
fichier.write (str(caisse))
fichier.close()

fichier = open("resultatshorse.csv", mode="w")
fichier.write (str(horse))
fichier.close()

fichier = open("resultatscycle.csv", mode="w")
fichier.write (str(cycle))
fichier.close()